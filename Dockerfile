FROM cypress/included:12.10.0
# Set the working directory
WORKDIR /TeamAPT Automation
# Copy the package.json and package-lock.json files to the container
COPY package*.json ./
# Install dependencies
RUN npm install
# Copy the rest of the application code to the container
COPY . .
EXPOSE 9000
# Run Cypress tests in headless mode, using the default environment configuration
CMD ["npm", "start"]


