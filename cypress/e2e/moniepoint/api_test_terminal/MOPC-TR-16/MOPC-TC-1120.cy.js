import MoniepointTester from '@moniepoint/cypress-tests-library/cypress/moniepoint_tester'
import BackOfficeLoginPage from '../../../../support/pages/back_office_login'
import BackOfficeHomePage from '../../../../support/pages/back_office_home'

const projectId = 10193
const testCycleKey = 'MOPC-TR-16'
const testCaseKey = 'MOPC-TC-1120'
const testName = testCycleKey + '-' + testCaseKey


function runTest() {
    const mt = new MoniepointTester(projectId, testCycleKey, testCaseKey)
    mt.describeWrapper(() => {
        beforeEach(() => {
            cy.task('createDatabaseConnections',Cypress.env('databaseConfigs'))
            cy.task('executeDatabaseQuery', { poolName: 'test', query: 'select * from tbl', values: [] })
        })

        it(testName, function() {
            cy.initWrap(mt, this).then((testCase) => {
                let backOfficeLoginPage, backOfficeHomePage
                testCase
                    .addStep({
                        testStepNo: 1,
                        action: function() {
                            backOfficeLoginPage = new BackOfficeLoginPage(Cypress.env('back_office_login_url'))
                            backOfficeLoginPage.login(Cypress.env('back_office_username'), Cypress.env('back_office_password'))
                        }
                    })
                    .addStep({
                        testStepNo: 2,
                        action: function() {
                            backOfficeHomePage = new BackOfficeHomePage()
                            backOfficeHomePage.clickConfiguration()
                        }
                    })
                    .addStep({
                        testStepNo: 3,
                        action: function() {
                            backOfficeHomePage.systemConfig()
                        }
                    })
                    .addStep({
                        testStepNo: 4,
                        action: function() {
                            backOfficeHomePage.search()
                        }
                    })
                    .executeSteps()
            })
        })
        
        afterEach(() => {
            cy.task('executeDatabaseQuery',{poolName: 'test',query: 'delete from tbl',values: []})
        })
    })

   
}
runTest()