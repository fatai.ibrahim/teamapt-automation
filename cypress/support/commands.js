//This file is for importing the custom commands created in the library
import CustomCommands from '@moniepoint/cypress-tests-library/cypress/custom_commands'
const customCommands=new CustomCommands()
Cypress.Commands.add('backOfficeLogin', customCommands.backOfficeLogin)
Cypress.Commands.add('initWrap',customCommands.initWrap)


