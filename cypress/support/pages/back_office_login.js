import MoniepointBasePage from '@moniepoint/cypress-tests-library/models/moniepoint_base_page'
import PageElement from '@moniepoint/cypress-tests-library/models/page_element'

class BackOfficeLoginPage extends MoniepointBasePage {
  constructor(baseUrl) {
    const pageElements = {
      loginWithEmailButton: ()=> new PageElement('a[href="/login/oauth2/cosmos"]'),
      usernameInput: () => new PageElement('input[name="username"]'),
      passwordInput: () => new PageElement('input[name="password"]'),
      loginButton: () => new PageElement('button','contains',"Sign In")
    }
    super(pageElements, baseUrl)

  }

  login(username, password) {
    this.pageElements.loginWithEmailButton().action('click')
    this.pageElements.usernameInput().action('type', username)
    this.pageElements.passwordInput().action('type', password)
    this.pageElements.loginButton().action('click')
    cy.url().should('contain', '#/home/dashboard')
  }
}

module.exports = BackOfficeLoginPage
