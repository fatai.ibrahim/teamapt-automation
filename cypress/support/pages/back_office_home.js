import MoniepointBasePage from '@moniepoint/cypress-tests-library/models/moniepoint_base_page'
import PageElement from '@moniepoint/cypress-tests-library/models/page_element'

class BackOfficeHomePage extends MoniepointBasePage {
  constructor() {
    const pageElements = {
      configurationTitle: ()=> new PageElement('[data-action-title="Configurations"]'),
      configurationSubMenu: () => new PageElement('[data-action-title="Configurations"] > ul.submenu'),
      systemConfig: () => new PageElement('[data-action-title="Configurations"] > ul.submenu > li >a[href="#/home/system-configurations"]'),
      searchInput:()=>new PageElement('input[placeholder="Input Key"]'),
      searchButton: () => new PageElement('a','contains',('search', { matchCase: false }))
    }
    super(pageElements)

  }

  clickConfiguration() {
    this.pageElements.configurationTitle().action('click')
    this.pageElements.configurationSubMenu().action('should', 'have.css', 'display', 'block')
  }
  systemConfig(){
    this.pageElements.systemConfig().action('click')
    cy.url().should('include', '#/home/system-configurations#pageHeader')
  }

  search(){
    this.pageElements.searchInput().action('type','protected-transfer.acceptance.blacklist.bank-account-number')
    this.pageElements.searchButton().action('should','be.visible','click')
  }
}


module.exports = BackOfficeHomePage

