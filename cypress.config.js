const DatabaseUtility = require("@moniepoint/cypress-tests-library/utilities/database")
const { defineConfig } = require("cypress")
const fs = require("fs-extra")
const path = require("path")

const fetchConfigurationByFile = file => {
  const pathOfConfigurationFile = `/cypress/config/cypress.${file}.json`

  return (
    file && fs.readJson(path.join(__dirname, pathOfConfigurationFile))
  )
}


module.exports = defineConfig({
  e2e: {
    video: true,
    chromeWebSecurity: false, //disables same origin policy on chrome
    setupNodeEvents(on, config) {
      const environment = config.env.configFile || "main"
      var configurationForEnvironment = fetchConfigurationByFile(environment)

      on('task', {
        createDatabaseConnections(databaseConfigs) {
          DatabaseUtility.createConnectionPools(databaseConfigs)
          return null
        },
        executeDatabaseQuery(options) {
          const { poolName, query, values } = options
          return DatabaseUtility.executeQuery(poolName, query, values)
        }
      })

      on('after:run', (results) => {
        return results
      })

      return configurationForEnvironment || config
    }
  }
})
